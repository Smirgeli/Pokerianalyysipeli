﻿namespace Pokerianalyysipeli
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Pelaa_nappula = new System.Windows.Forms.Button();
            this.Lopeta_nappula = new System.Windows.Forms.Button();
            this.Teksti1 = new System.Windows.Forms.TextBox();
            this.Pelaaja1Label = new System.Windows.Forms.Label();
            this.Pelaaja2Label = new System.Windows.Forms.Label();
            this.Teksti2 = new System.Windows.Forms.TextBox();
            this.Pelaaja3Label = new System.Windows.Forms.Label();
            this.Teksti3 = new System.Windows.Forms.TextBox();
            this.Pelaaja4Label = new System.Windows.Forms.Label();
            this.Teksti4 = new System.Windows.Forms.TextBox();
            this.Pelaaja5Label = new System.Windows.Forms.Label();
            this.Teksti5 = new System.Windows.Forms.TextBox();
            this.Pelaaja10Label = new System.Windows.Forms.Label();
            this.Teksti10 = new System.Windows.Forms.TextBox();
            this.Pelaaja9Label = new System.Windows.Forms.Label();
            this.Teksti9 = new System.Windows.Forms.TextBox();
            this.Pelaaja8Label = new System.Windows.Forms.Label();
            this.Teksti8 = new System.Windows.Forms.TextBox();
            this.Pelaaja7Label = new System.Windows.Forms.Label();
            this.Teksti7 = new System.Windows.Forms.TextBox();
            this.Pelaaja6Label = new System.Windows.Forms.Label();
            this.Teksti6 = new System.Windows.Forms.TextBox();
            this.VoittajaLabel = new System.Windows.Forms.Label();
            this.voittajaText = new System.Windows.Forms.TextBox();
            this.voittoText = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.kasi5text = new System.Windows.Forms.TextBox();
            this.kasi1text = new System.Windows.Forms.TextBox();
            this.kasi2text = new System.Windows.Forms.TextBox();
            this.kasi3text = new System.Windows.Forms.TextBox();
            this.kasi4text = new System.Windows.Forms.TextBox();
            this.kasi9text = new System.Windows.Forms.TextBox();
            this.kasi8text = new System.Windows.Forms.TextBox();
            this.kasi7text = new System.Windows.Forms.TextBox();
            this.kasi6text = new System.Windows.Forms.TextBox();
            this.kasi10text = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // Pelaa_nappula
            // 
            this.Pelaa_nappula.Location = new System.Drawing.Point(393, 359);
            this.Pelaa_nappula.Name = "Pelaa_nappula";
            this.Pelaa_nappula.Size = new System.Drawing.Size(128, 23);
            this.Pelaa_nappula.TabIndex = 0;
            this.Pelaa_nappula.Text = "Pelaa";
            this.Pelaa_nappula.UseVisualStyleBackColor = true;
            this.Pelaa_nappula.Click += new System.EventHandler(this.Pelaa_nappula_Click);
            // 
            // Lopeta_nappula
            // 
            this.Lopeta_nappula.Location = new System.Drawing.Point(528, 408);
            this.Lopeta_nappula.Name = "Lopeta_nappula";
            this.Lopeta_nappula.Size = new System.Drawing.Size(75, 23);
            this.Lopeta_nappula.TabIndex = 1;
            this.Lopeta_nappula.Text = "Lopeta";
            this.Lopeta_nappula.UseVisualStyleBackColor = true;
            this.Lopeta_nappula.Visible = false;
            // 
            // Teksti1
            // 
            this.Teksti1.BackColor = System.Drawing.SystemColors.HighlightText;
            this.Teksti1.ForeColor = System.Drawing.SystemColors.MenuText;
            this.Teksti1.Location = new System.Drawing.Point(81, 79);
            this.Teksti1.Name = "Teksti1";
            this.Teksti1.ReadOnly = true;
            this.Teksti1.Size = new System.Drawing.Size(249, 20);
            this.Teksti1.TabIndex = 2;
            this.Teksti1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // Pelaaja1Label
            // 
            this.Pelaaja1Label.AutoSize = true;
            this.Pelaaja1Label.Location = new System.Drawing.Point(168, 42);
            this.Pelaaja1Label.Name = "Pelaaja1Label";
            this.Pelaaja1Label.Size = new System.Drawing.Size(58, 13);
            this.Pelaaja1Label.TabIndex = 3;
            this.Pelaaja1Label.Text = "Pelaaja #1";
            // 
            // Pelaaja2Label
            // 
            this.Pelaaja2Label.AutoSize = true;
            this.Pelaaja2Label.Location = new System.Drawing.Point(168, 104);
            this.Pelaaja2Label.Name = "Pelaaja2Label";
            this.Pelaaja2Label.Size = new System.Drawing.Size(58, 13);
            this.Pelaaja2Label.TabIndex = 5;
            this.Pelaaja2Label.Text = "Pelaaja #2";
            // 
            // Teksti2
            // 
            this.Teksti2.BackColor = System.Drawing.SystemColors.HighlightText;
            this.Teksti2.ForeColor = System.Drawing.SystemColors.MenuText;
            this.Teksti2.Location = new System.Drawing.Point(81, 141);
            this.Teksti2.Name = "Teksti2";
            this.Teksti2.ReadOnly = true;
            this.Teksti2.Size = new System.Drawing.Size(249, 20);
            this.Teksti2.TabIndex = 4;
            this.Teksti2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // Pelaaja3Label
            // 
            this.Pelaaja3Label.AutoSize = true;
            this.Pelaaja3Label.Location = new System.Drawing.Point(168, 165);
            this.Pelaaja3Label.Name = "Pelaaja3Label";
            this.Pelaaja3Label.Size = new System.Drawing.Size(58, 13);
            this.Pelaaja3Label.TabIndex = 7;
            this.Pelaaja3Label.Text = "Pelaaja #3";
            // 
            // Teksti3
            // 
            this.Teksti3.BackColor = System.Drawing.SystemColors.HighlightText;
            this.Teksti3.ForeColor = System.Drawing.SystemColors.MenuText;
            this.Teksti3.Location = new System.Drawing.Point(81, 202);
            this.Teksti3.Name = "Teksti3";
            this.Teksti3.ReadOnly = true;
            this.Teksti3.Size = new System.Drawing.Size(249, 20);
            this.Teksti3.TabIndex = 6;
            this.Teksti3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // Pelaaja4Label
            // 
            this.Pelaaja4Label.AutoSize = true;
            this.Pelaaja4Label.Location = new System.Drawing.Point(168, 222);
            this.Pelaaja4Label.Name = "Pelaaja4Label";
            this.Pelaaja4Label.Size = new System.Drawing.Size(58, 13);
            this.Pelaaja4Label.TabIndex = 9;
            this.Pelaaja4Label.Text = "Pelaaja #4";
            // 
            // Teksti4
            // 
            this.Teksti4.BackColor = System.Drawing.SystemColors.HighlightText;
            this.Teksti4.ForeColor = System.Drawing.SystemColors.MenuText;
            this.Teksti4.Location = new System.Drawing.Point(81, 259);
            this.Teksti4.Name = "Teksti4";
            this.Teksti4.ReadOnly = true;
            this.Teksti4.Size = new System.Drawing.Size(249, 20);
            this.Teksti4.TabIndex = 8;
            this.Teksti4.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // Pelaaja5Label
            // 
            this.Pelaaja5Label.AutoSize = true;
            this.Pelaaja5Label.Location = new System.Drawing.Point(168, 286);
            this.Pelaaja5Label.Name = "Pelaaja5Label";
            this.Pelaaja5Label.Size = new System.Drawing.Size(58, 13);
            this.Pelaaja5Label.TabIndex = 11;
            this.Pelaaja5Label.Text = "Pelaaja #5";
            // 
            // Teksti5
            // 
            this.Teksti5.BackColor = System.Drawing.SystemColors.HighlightText;
            this.Teksti5.ForeColor = System.Drawing.SystemColors.MenuText;
            this.Teksti5.Location = new System.Drawing.Point(81, 323);
            this.Teksti5.Name = "Teksti5";
            this.Teksti5.ReadOnly = true;
            this.Teksti5.Size = new System.Drawing.Size(249, 20);
            this.Teksti5.TabIndex = 10;
            this.Teksti5.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // Pelaaja10Label
            // 
            this.Pelaaja10Label.AutoSize = true;
            this.Pelaaja10Label.Location = new System.Drawing.Point(677, 286);
            this.Pelaaja10Label.Name = "Pelaaja10Label";
            this.Pelaaja10Label.Size = new System.Drawing.Size(64, 13);
            this.Pelaaja10Label.TabIndex = 21;
            this.Pelaaja10Label.Text = "Pelaaja #10";
            // 
            // Teksti10
            // 
            this.Teksti10.BackColor = System.Drawing.SystemColors.HighlightText;
            this.Teksti10.ForeColor = System.Drawing.SystemColors.MenuText;
            this.Teksti10.Location = new System.Drawing.Point(592, 323);
            this.Teksti10.Name = "Teksti10";
            this.Teksti10.ReadOnly = true;
            this.Teksti10.Size = new System.Drawing.Size(240, 20);
            this.Teksti10.TabIndex = 20;
            this.Teksti10.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // Pelaaja9Label
            // 
            this.Pelaaja9Label.AutoSize = true;
            this.Pelaaja9Label.Location = new System.Drawing.Point(677, 222);
            this.Pelaaja9Label.Name = "Pelaaja9Label";
            this.Pelaaja9Label.Size = new System.Drawing.Size(58, 13);
            this.Pelaaja9Label.TabIndex = 19;
            this.Pelaaja9Label.Text = "Pelaaja #9";
            // 
            // Teksti9
            // 
            this.Teksti9.BackColor = System.Drawing.SystemColors.HighlightText;
            this.Teksti9.ForeColor = System.Drawing.SystemColors.MenuText;
            this.Teksti9.Location = new System.Drawing.Point(592, 259);
            this.Teksti9.Name = "Teksti9";
            this.Teksti9.ReadOnly = true;
            this.Teksti9.Size = new System.Drawing.Size(240, 20);
            this.Teksti9.TabIndex = 18;
            this.Teksti9.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // Pelaaja8Label
            // 
            this.Pelaaja8Label.AutoSize = true;
            this.Pelaaja8Label.Location = new System.Drawing.Point(677, 165);
            this.Pelaaja8Label.Name = "Pelaaja8Label";
            this.Pelaaja8Label.Size = new System.Drawing.Size(58, 13);
            this.Pelaaja8Label.TabIndex = 17;
            this.Pelaaja8Label.Text = "Pelaaja #8";
            // 
            // Teksti8
            // 
            this.Teksti8.BackColor = System.Drawing.SystemColors.HighlightText;
            this.Teksti8.ForeColor = System.Drawing.SystemColors.MenuText;
            this.Teksti8.Location = new System.Drawing.Point(592, 202);
            this.Teksti8.Name = "Teksti8";
            this.Teksti8.ReadOnly = true;
            this.Teksti8.Size = new System.Drawing.Size(240, 20);
            this.Teksti8.TabIndex = 16;
            this.Teksti8.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // Pelaaja7Label
            // 
            this.Pelaaja7Label.AutoSize = true;
            this.Pelaaja7Label.Location = new System.Drawing.Point(677, 104);
            this.Pelaaja7Label.Name = "Pelaaja7Label";
            this.Pelaaja7Label.Size = new System.Drawing.Size(58, 13);
            this.Pelaaja7Label.TabIndex = 15;
            this.Pelaaja7Label.Text = "Pelaaja #7";
            // 
            // Teksti7
            // 
            this.Teksti7.BackColor = System.Drawing.SystemColors.HighlightText;
            this.Teksti7.ForeColor = System.Drawing.SystemColors.MenuText;
            this.Teksti7.Location = new System.Drawing.Point(592, 141);
            this.Teksti7.Name = "Teksti7";
            this.Teksti7.ReadOnly = true;
            this.Teksti7.Size = new System.Drawing.Size(240, 20);
            this.Teksti7.TabIndex = 14;
            this.Teksti7.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // Pelaaja6Label
            // 
            this.Pelaaja6Label.AutoSize = true;
            this.Pelaaja6Label.Location = new System.Drawing.Point(677, 42);
            this.Pelaaja6Label.Name = "Pelaaja6Label";
            this.Pelaaja6Label.Size = new System.Drawing.Size(58, 13);
            this.Pelaaja6Label.TabIndex = 13;
            this.Pelaaja6Label.Text = "Pelaaja #6";
            // 
            // Teksti6
            // 
            this.Teksti6.BackColor = System.Drawing.SystemColors.HighlightText;
            this.Teksti6.ForeColor = System.Drawing.SystemColors.MenuText;
            this.Teksti6.Location = new System.Drawing.Point(592, 79);
            this.Teksti6.Name = "Teksti6";
            this.Teksti6.ReadOnly = true;
            this.Teksti6.Size = new System.Drawing.Size(240, 20);
            this.Teksti6.TabIndex = 12;
            this.Teksti6.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // VoittajaLabel
            // 
            this.VoittajaLabel.AutoSize = true;
            this.VoittajaLabel.Location = new System.Drawing.Point(432, 159);
            this.VoittajaLabel.Name = "VoittajaLabel";
            this.VoittajaLabel.Size = new System.Drawing.Size(48, 13);
            this.VoittajaLabel.TabIndex = 23;
            this.VoittajaLabel.Text = "Voittajat:";
            // 
            // voittajaText
            // 
            this.voittajaText.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.voittajaText.Location = new System.Drawing.Point(374, 175);
            this.voittajaText.Multiline = true;
            this.voittajaText.Name = "voittajaText";
            this.voittajaText.ReadOnly = true;
            this.voittajaText.Size = new System.Drawing.Size(170, 59);
            this.voittajaText.TabIndex = 24;
            this.voittajaText.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // voittoText
            // 
            this.voittoText.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.voittoText.Location = new System.Drawing.Point(374, 97);
            this.voittoText.Multiline = true;
            this.voittoText.Name = "voittoText";
            this.voittoText.ReadOnly = true;
            this.voittoText.Size = new System.Drawing.Size(170, 20);
            this.voittoText.TabIndex = 26;
            this.voittoText.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(427, 81);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(56, 13);
            this.label1.TabIndex = 25;
            this.label1.Text = "Voittokäsi:";
            // 
            // kasi5text
            // 
            this.kasi5text.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.kasi5text.Location = new System.Drawing.Point(144, 302);
            this.kasi5text.Name = "kasi5text";
            this.kasi5text.ReadOnly = true;
            this.kasi5text.Size = new System.Drawing.Size(100, 20);
            this.kasi5text.TabIndex = 0;
            this.kasi5text.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // kasi1text
            // 
            this.kasi1text.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.kasi1text.Location = new System.Drawing.Point(144, 58);
            this.kasi1text.Name = "kasi1text";
            this.kasi1text.ReadOnly = true;
            this.kasi1text.Size = new System.Drawing.Size(100, 20);
            this.kasi1text.TabIndex = 27;
            this.kasi1text.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // kasi2text
            // 
            this.kasi2text.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.kasi2text.Location = new System.Drawing.Point(144, 120);
            this.kasi2text.Name = "kasi2text";
            this.kasi2text.ReadOnly = true;
            this.kasi2text.Size = new System.Drawing.Size(100, 20);
            this.kasi2text.TabIndex = 28;
            this.kasi2text.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // kasi3text
            // 
            this.kasi3text.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.kasi3text.Location = new System.Drawing.Point(144, 181);
            this.kasi3text.Name = "kasi3text";
            this.kasi3text.ReadOnly = true;
            this.kasi3text.Size = new System.Drawing.Size(100, 20);
            this.kasi3text.TabIndex = 29;
            this.kasi3text.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // kasi4text
            // 
            this.kasi4text.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.kasi4text.Location = new System.Drawing.Point(144, 238);
            this.kasi4text.Name = "kasi4text";
            this.kasi4text.ReadOnly = true;
            this.kasi4text.Size = new System.Drawing.Size(100, 20);
            this.kasi4text.TabIndex = 30;
            this.kasi4text.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // kasi9text
            // 
            this.kasi9text.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.kasi9text.Location = new System.Drawing.Point(652, 238);
            this.kasi9text.Name = "kasi9text";
            this.kasi9text.ReadOnly = true;
            this.kasi9text.Size = new System.Drawing.Size(100, 20);
            this.kasi9text.TabIndex = 35;
            this.kasi9text.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // kasi8text
            // 
            this.kasi8text.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.kasi8text.Location = new System.Drawing.Point(652, 181);
            this.kasi8text.Name = "kasi8text";
            this.kasi8text.ReadOnly = true;
            this.kasi8text.Size = new System.Drawing.Size(100, 20);
            this.kasi8text.TabIndex = 34;
            this.kasi8text.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // kasi7text
            // 
            this.kasi7text.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.kasi7text.Location = new System.Drawing.Point(652, 120);
            this.kasi7text.Name = "kasi7text";
            this.kasi7text.ReadOnly = true;
            this.kasi7text.Size = new System.Drawing.Size(100, 20);
            this.kasi7text.TabIndex = 33;
            this.kasi7text.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // kasi6text
            // 
            this.kasi6text.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.kasi6text.Location = new System.Drawing.Point(652, 58);
            this.kasi6text.Name = "kasi6text";
            this.kasi6text.ReadOnly = true;
            this.kasi6text.Size = new System.Drawing.Size(100, 20);
            this.kasi6text.TabIndex = 32;
            this.kasi6text.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // kasi10text
            // 
            this.kasi10text.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.kasi10text.Location = new System.Drawing.Point(652, 302);
            this.kasi10text.Name = "kasi10text";
            this.kasi10text.ReadOnly = true;
            this.kasi10text.Size = new System.Drawing.Size(100, 20);
            this.kasi10text.TabIndex = 31;
            this.kasi10text.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(907, 529);
            this.Controls.Add(this.kasi9text);
            this.Controls.Add(this.kasi8text);
            this.Controls.Add(this.kasi7text);
            this.Controls.Add(this.kasi6text);
            this.Controls.Add(this.kasi10text);
            this.Controls.Add(this.kasi4text);
            this.Controls.Add(this.kasi3text);
            this.Controls.Add(this.kasi2text);
            this.Controls.Add(this.kasi1text);
            this.Controls.Add(this.kasi5text);
            this.Controls.Add(this.voittoText);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.voittajaText);
            this.Controls.Add(this.VoittajaLabel);
            this.Controls.Add(this.Pelaaja10Label);
            this.Controls.Add(this.Teksti10);
            this.Controls.Add(this.Pelaaja9Label);
            this.Controls.Add(this.Teksti9);
            this.Controls.Add(this.Pelaaja8Label);
            this.Controls.Add(this.Teksti8);
            this.Controls.Add(this.Pelaaja7Label);
            this.Controls.Add(this.Teksti7);
            this.Controls.Add(this.Pelaaja6Label);
            this.Controls.Add(this.Teksti6);
            this.Controls.Add(this.Pelaaja5Label);
            this.Controls.Add(this.Teksti5);
            this.Controls.Add(this.Pelaaja4Label);
            this.Controls.Add(this.Teksti4);
            this.Controls.Add(this.Pelaaja3Label);
            this.Controls.Add(this.Teksti3);
            this.Controls.Add(this.Pelaaja2Label);
            this.Controls.Add(this.Teksti2);
            this.Controls.Add(this.Pelaaja1Label);
            this.Controls.Add(this.Teksti1);
            this.Controls.Add(this.Lopeta_nappula);
            this.Controls.Add(this.Pelaa_nappula);
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Pokerianalyysipeli";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainForm_FormClosing);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button Pelaa_nappula;
        private System.Windows.Forms.Button Lopeta_nappula;
        private System.Windows.Forms.TextBox Teksti1;
        private System.Windows.Forms.Label Pelaaja1Label;
        private System.Windows.Forms.Label Pelaaja2Label;
        private System.Windows.Forms.TextBox Teksti2;
        private System.Windows.Forms.Label Pelaaja3Label;
        private System.Windows.Forms.TextBox Teksti3;
        private System.Windows.Forms.Label Pelaaja4Label;
        private System.Windows.Forms.TextBox Teksti4;
        private System.Windows.Forms.Label Pelaaja5Label;
        private System.Windows.Forms.TextBox Teksti5;
        private System.Windows.Forms.Label Pelaaja10Label;
        private System.Windows.Forms.TextBox Teksti10;
        private System.Windows.Forms.Label Pelaaja9Label;
        private System.Windows.Forms.TextBox Teksti9;
        private System.Windows.Forms.Label Pelaaja8Label;
        private System.Windows.Forms.TextBox Teksti8;
        private System.Windows.Forms.Label Pelaaja7Label;
        private System.Windows.Forms.TextBox Teksti7;
        private System.Windows.Forms.Label Pelaaja6Label;
        private System.Windows.Forms.TextBox Teksti6;
        private System.Windows.Forms.Label VoittajaLabel;
        private System.Windows.Forms.TextBox voittajaText;
        private System.Windows.Forms.TextBox voittoText;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox kasi5text;
        private System.Windows.Forms.TextBox kasi1text;
        private System.Windows.Forms.TextBox kasi2text;
        private System.Windows.Forms.TextBox kasi3text;
        private System.Windows.Forms.TextBox kasi4text;
        private System.Windows.Forms.TextBox kasi9text;
        private System.Windows.Forms.TextBox kasi8text;
        private System.Windows.Forms.TextBox kasi7text;
        private System.Windows.Forms.TextBox kasi6text;
        private System.Windows.Forms.TextBox kasi10text;
    }
}