﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Pokerianalyysipeli
{
    public partial class MainForm : Form
    {
        Poyta poyta = new Poyta();
        private int lukum;
        private List<int> voittajat = new List<int>();

        public MainForm(int lkm)
        {
            InitializeComponent();
            poyta.AsetaPelaajat(lkm);
            lukum = lkm;
        }

        private void Pelaa_nappula_Click(object sender, EventArgs e)
        {
            poyta.Korttienjako();
            poyta.tulostaKortit();
            voittajaText.Text = "";
            //Koska pokerissa pelataan vain yhdellä pakalla pelaajia voi olla maksimissaan 10 ja Formissa oli useampi teksti laatikko näiden lisäksi ns."kovakoodasin" kaikki 10 erikseen
            if (lukum >= 1)
            {
                Teksti1.Text = poyta.getKasikortit(0);
            }
            if (lukum >= 2)
            {
                Teksti2.Text = poyta.getKasikortit(1);
            }
            if (lukum >= 3)
            {
                Teksti3.Text = poyta.getKasikortit(2);
            }
            if (lukum >= 4)
            {
                Teksti4.Text = poyta.getKasikortit(3);
            }
            if (lukum >= 5)
            {
                Teksti5.Text = poyta.getKasikortit(4);
            }
            if (lukum >= 6)
            {
                Teksti6.Text = poyta.getKasikortit(5);
            }
            if (lukum >= 7)
            {
                Teksti7.Text = poyta.getKasikortit(6);
            }
            if (lukum >= 8)
            {
                Teksti8.Text = poyta.getKasikortit(7);
            }
            if (lukum >= 9)
            {
                Teksti9.Text = poyta.getKasikortit(8);
            }
            if (lukum >= 10)
            {
                Teksti10.Text = poyta.getKasikortit(9);
            }

            poyta.tyhjennaLista();
            poyta.Analyysi();

            if (lukum >= 1)
            {
                kasi1text.Text = poyta.getVoitto(0);
            }
            if (lukum >= 2)
            {
                kasi2text.Text = poyta.getVoitto(1);
            }
            if (lukum >= 3)
            {
                kasi3text.Text = poyta.getVoitto(2);
            }
            if (lukum >= 4)
            {
                kasi4text.Text = poyta.getVoitto(3);
            }
            if (lukum >= 5)
            {
                kasi5text.Text = poyta.getVoitto(4);
            }
            if (lukum >= 6)
            {
                kasi6text.Text = poyta.getVoitto(5);
            }
            if (lukum >= 7)
            {
                kasi7text.Text = poyta.getVoitto(6);
            }
            if (lukum >= 8)
            {
                kasi8text.Text = poyta.getVoitto(7);
            }
            if (lukum >= 9)
            {
                kasi9text.Text = poyta.getVoitto(8);
            }
            if (lukum >= 10)
            {
                kasi10text.Text = poyta.getVoitto(9);
            }

            poyta.tyhjennaKasilista();
            voittajat = poyta.getMuisti();
            voittoText.Text = poyta.getVoittokasi();

            for (int l = 0; l < voittajat.Count; l++)
            {
                voittajaText.Text = voittajaText.Text + "Pelaaja #" + voittajat[l] + " "; 
            }

            poyta.tyhjennaMuistivali();
            voittajat.Clear();
            
        }

        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            Application.Exit();
        }
    }
}
