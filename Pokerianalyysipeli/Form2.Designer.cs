﻿namespace Pokerianalyysipeli
{
    partial class SubForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.SubFormText = new System.Windows.Forms.TextBox();
            this.pelaaja_label = new System.Windows.Forms.Label();
            this.OK_nappula = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // SubFormText
            // 
            this.SubFormText.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.SubFormText.Location = new System.Drawing.Point(127, 71);
            this.SubFormText.Name = "SubFormText";
            this.SubFormText.Size = new System.Drawing.Size(100, 20);
            this.SubFormText.TabIndex = 0;
            // 
            // pelaaja_label
            // 
            this.pelaaja_label.AutoSize = true;
            this.pelaaja_label.Location = new System.Drawing.Point(107, 55);
            this.pelaaja_label.Name = "pelaaja_label";
            this.pelaaja_label.Size = new System.Drawing.Size(138, 13);
            this.pelaaja_label.TabIndex = 1;
            this.pelaaja_label.Text = "Pelaajien lukumäärä? (1-10)";
            // 
            // OK_nappula
            // 
            this.OK_nappula.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.OK_nappula.Location = new System.Drawing.Point(141, 97);
            this.OK_nappula.Name = "OK_nappula";
            this.OK_nappula.Size = new System.Drawing.Size(75, 23);
            this.OK_nappula.TabIndex = 2;
            this.OK_nappula.Text = "OK";
            this.OK_nappula.UseVisualStyleBackColor = true;
            this.OK_nappula.Click += new System.EventHandler(this.OK_nappula_Click);
            // 
            // SubForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(339, 164);
            this.Controls.Add(this.OK_nappula);
            this.Controls.Add(this.pelaaja_label);
            this.Controls.Add(this.SubFormText);
            this.Name = "SubForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Pokerianalyysipeli";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.SubForm_FormClosing);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox SubFormText;
        private System.Windows.Forms.Label pelaaja_label;
        private System.Windows.Forms.Button OK_nappula;
    }
}