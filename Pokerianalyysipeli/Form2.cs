﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Pokerianalyysipeli
{
    public partial class SubForm : Form
    {
        
        private int lkm;

        public SubForm()
        {
            InitializeComponent();
        }

        private void SubForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            Application.Exit();
        }

        private void OK_nappula_Click(object sender, EventArgs e)
        {
            if (int.TryParse(SubFormText.Text, out lkm))
            {
                if (lkm <= 10 && lkm > 0)
                {
                    //poyta.AsetaPelaajat(lkm);
                    MainForm maini = new MainForm(lkm);
                    maini.Show();
                    this.Hide();
                }
                else
                {
                    //Console.WriteLine("Anna numero 1-10!");
                    MessageBox.Show("Anna numero 1-10!");
                }
            }
            else
            {
                //Console.WriteLine("Anna numero 1-10!");
                MessageBox.Show("Anna numero 1-10!");
            }
        }
        public int getLKM()
        {
            return lkm;
        }

    }
}
