﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pokerianalyysipeli
{
    class Jakaja
    {
        private Korttipakka kp;
        private Kortti[] kortit = new Kortti[5];
        private int voitto = 0;
        private String voittokasi;
        private int muistavoitto = 0;
        private String tulos = "";
        //Kun Jakajan alustaa niin korttipakkojen määrä pitää tietää
        public Jakaja(int pakkojenmaara)
        {
            kp = new Korttipakka(pakkojenmaara);
        }
        
        //käytännössä vain ajaa Korttipakan Sekoita metodin
        public void SekoitaKortit()
        {
            kp.Sekoita();
        }

        //ottaa koko korttipakan sisällön Korttipakka luokasta ja palauttaa sen kutsuessa
        public Kortti[] getKortit()
        {
            return kp.GetPakka();

        }

        private void setVoitto(int q)
        {
            voitto = q;
        }
        public int getVoitto()
        {
            return voitto;
        }
        public String getVoittokasi()
        {
            return voittokasi;
        }
        public void tyhjennaMuistavoitto()
        {
            muistavoitto = 0;
        }
        //tarkistaa ja vertaa kortteja toisiinsa ja palauttaa pokerikäden nimen, sekä asettaa voiton arvon, jolla verrataan kenen käsi voitti
        public String TeeAnalyysi(Kortti[] kortti)
        {


            if (kortti[0].getArvo() != kortti[1].getArvo() && kortti[1].getArvo() != kortti[2].getArvo() && kortti[2].getArvo() != kortti[3].getArvo() && kortti[3].getArvo() != kortti[4].getArvo())
            {

                if (kortti[0].getArvo() == 1)
                {
                    tulos = "A Hai";
                    setVoitto(kortti[0].getArvo()+13);
                }
                else if (kortti[4].getArvo() == 11)
                {
                    tulos = "J Hai";
                    setVoitto(kortti[4].getArvo());
                }
                else if (kortti[4].getArvo() == 12)
                {
                    tulos = "Q Hai";
                    setVoitto(kortti[4].getArvo());
                }
                else if (kortti[4].getArvo() == 13)
                {
                    tulos = "K Hai";
                    setVoitto(kortti[4].getArvo());
                }
                else
                {
                    tulos = kortti[4].getArvo() + " Hai";
                    setVoitto(kortti[4].getArvo());
                }
            }            
            if (kortti[0].getArvo() == kortti[1].getArvo() || kortti[1].getArvo() == kortti[2].getArvo() || kortti[2].getArvo() == kortti[3].getArvo() || kortti[3].getArvo() == kortti[4].getArvo())
            {
                if (kortti[0].getArvo() == kortti[1].getArvo())
                {
                    if (kortti[0].getArvo() < 11 && kortti[0].getArvo() > 1)
                    {
                        tulos = kortti[0].getArvo() + " pari";
                        setVoitto(kortti[0].getArvo() + 20);
                    }
                    else if (kortti[0].getArvo() == 11)
                    {
                        tulos = "J pari";
                        setVoitto(kortti[0].getArvo() + 20);
                    }
                    else if (kortti[0].getArvo() == 12)
                    {
                        tulos = "Q pari";
                        setVoitto(kortti[0].getArvo() + 20);
                    }
                    else if (kortti[0].getArvo() == 13)
                    {
                        tulos = "K pari";
                        setVoitto(kortti[0].getArvo() + 20);
                    }
                    else if (kortti[0].getArvo() == 1)
                    {
                        tulos = "A pari";
                        setVoitto(kortti[0].getArvo() + 20 + 13);
                    }
                }
                else if (kortti[1].getArvo() == kortti[2].getArvo())
                {
                    if (kortti[1].getArvo() < 11 && kortti[1].getArvo() > 1)
                    {
                        tulos = kortti[1].getArvo() + " pari";
                        setVoitto(kortti[1].getArvo() + 20);
                    }
                    else if (kortti[1].getArvo() == 11)
                    {
                        tulos = "J pari";
                        setVoitto(kortti[1].getArvo() + 20);
                    }
                    else if (kortti[1].getArvo() == 12)
                    {
                        tulos = "Q pari";
                        setVoitto(kortti[1].getArvo() + 20);
                    }
                    else if (kortti[1].getArvo() == 13)
                    {
                        tulos = "K pari";
                        setVoitto(kortti[1].getArvo() + 20);
                    }
                    else if (kortti[1].getArvo() == 1)
                    {
                        tulos = "A pari";
                        setVoitto(kortti[1].getArvo() + 20 + 13);
                    }
                }
                else if (kortti[2].getArvo() == kortti[3].getArvo())
                {
                    if (kortti[2].getArvo() < 11 && kortti[2].getArvo() > 1)
                    {
                        tulos = kortti[2].getArvo() + " pari";
                        setVoitto(kortti[2].getArvo() + 20);
                    }
                    else if (kortti[2].getArvo() == 11)
                    {
                        tulos = "J pari";
                        setVoitto(kortti[2].getArvo() + 20);
                    }
                    else if (kortti[2].getArvo() == 12)
                    {
                        tulos = "Q pari";
                        setVoitto(kortti[2].getArvo() + 20);
                    }
                    else if (kortti[2].getArvo() == 13)
                    {
                        tulos = "K pari";
                        setVoitto(kortti[2].getArvo() + 20);
                    }
                    else if (kortti[2].getArvo() == 1)
                    {
                        tulos = "A pari";
                        setVoitto(kortti[2].getArvo() + 20 + 13);
                    }
                }
                else
                {
                    if (kortti[3].getArvo() < 11 && kortti[3].getArvo() > 1)
                    {
                        tulos = kortti[3].getArvo() + " pari";
                        setVoitto(kortti[3].getArvo() + 20);
                    }
                    else if (kortti[3].getArvo() == 11)
                    {
                        tulos = "J pari";
                        setVoitto(kortti[3].getArvo() + 20);
                    }
                    else if (kortti[3].getArvo() == 12)
                    {
                        tulos = "Q pari";
                        setVoitto(kortti[3].getArvo() + 20);
                    }
                    else if (kortti[3].getArvo() == 13)
                    {
                        tulos = "K pari";
                        setVoitto(kortti[3].getArvo() + 20);
                    }
                    else if (kortti[3].getArvo() == 1)
                    {
                        tulos = "A pari";
                        setVoitto(kortti[3].getArvo() + 20 + 13);
                    }
                }
            }
            if (kortti[0].getArvo() == kortti[1].getArvo() && kortti[2].getArvo() == kortti[3].getArvo() && kortti[1].getArvo() != kortti[2].getArvo() || kortti[1].getArvo() == kortti[2].getArvo() && kortti[3].getArvo() == kortti[4].getArvo() && kortti[2].getArvo() != kortti[3].getArvo() || kortti[0].getArvo() == kortti[1].getArvo() && kortti[3].getArvo() == kortti[4].getArvo() && kortti[2].getArvo() != kortti[3].getArvo() && kortti[1].getArvo() != kortti[2].getArvo())
            {
                tulos = "Kaksi paria";
                if (kortti[0].getArvo() == kortti[1].getArvo() && kortti[0].getArvo() == 1)
                {
                    setVoitto(kortti[3].getArvo() + 40 + 13);
                } else
                {
                    setVoitto(kortti[3].getArvo() + 40);
                }
            }
            if (kortti[0].getArvo() == kortti[1].getArvo() && kortti[0].getArvo() == kortti[2].getArvo() && kortti[3].getArvo() != kortti[4].getArvo() || kortti[1].getArvo() == kortti[2].getArvo() && kortti[1].getArvo() == kortti[3].getArvo() && kortti[0].getArvo() != kortti[1].getArvo() && kortti[4].getArvo() != kortti[3].getArvo() || kortti[1].getArvo() != kortti[2].getArvo() && kortti[2].getArvo() == kortti[3].getArvo() && kortti[2].getArvo() == kortti[4].getArvo())
            {
                tulos = "Kolmoset";
                if (kortti[0].getArvo() == kortti[1].getArvo() && kortti[0].getArvo() == kortti[2].getArvo() && kortti[0].getArvo() == 1)
                {
                    setVoitto(kortti[2].getArvo() + 60 + 13);
                }
                else
                {
                    setVoitto(kortti[2].getArvo() + 60);
                }
            }
            if (kortti[1].getArvo() == (kortti[0].getArvo() + 1) && kortti[2].getArvo() == (kortti[0].getArvo() + 2) && kortti[3].getArvo() == (kortti[0].getArvo() + 3) && kortti[4].getArvo() == (kortti[0].getArvo() + 4) || kortti[2].getArvo() == (kortti[1].getArvo() + 1) && kortti[3].getArvo() == (kortti[1].getArvo() + 2) && kortti[4].getArvo() == (kortti[1].getArvo() + 3) && kortti[4].getArvo() == (kortti[0].getArvo() + 12))
            {
                tulos = "Suora";
                if (kortti[0].getArvo() == 1)
                {
                    setVoitto(kortti[0].getArvo() + 80 + 13);
                }
                else
                {
                    setVoitto(kortti[4].getArvo() + 80);
                }
            }
            if (kortti[0].getMaa() == kortti[1].getMaa() && kortti[0].getMaa() == kortti[2].getMaa() && kortti[0].getMaa() == kortti[3].getMaa() && kortti[0].getMaa() == kortti[4].getMaa())
            {
                tulos = "Vari";
                if (kortti[0].getArvo() == 1)
                {
                    setVoitto(kortti[0].getArvo() + 100 + 13);
                }
                else
                {
                    setVoitto(kortti[4].getArvo() + 100);
                }

            }
            if (kortti[0].getArvo() == kortti[1].getArvo() && kortti[2].getArvo() == kortti[3].getArvo() && kortti[2].getArvo() == kortti[4].getArvo() || kortti[0].getArvo() == kortti[1].getArvo() && kortti[0].getArvo() == kortti[2].getArvo() && kortti[3].getArvo() == kortti[4].getArvo())
            {
                tulos = "Tayskasi";
                if (kortti[0].getArvo() == kortti[1].getArvo() && kortti[0].getArvo() == kortti[2].getArvo() && kortti[0].getArvo() == 1)
                {
                    setVoitto(kortti[0].getArvo() + 120 + 13);
                }
                else
                {
                    setVoitto(kortti[2].getArvo() + 120);
                }
            }
            if (kortti[0].getArvo() == kortti[1].getArvo() && kortti[0].getArvo() == kortti[2].getArvo() && kortti[0].getArvo() == kortti[3].getArvo() || kortti[1].getArvo() == kortti[2].getArvo() && kortti[1].getArvo() == kortti[3].getArvo() && kortti[1].getArvo() == kortti[4].getArvo())
            {
                tulos = "Neloset";
                if (kortti[0].getArvo() == kortti[1].getArvo() && kortti[0].getArvo() == kortti[2].getArvo() && kortti[0].getArvo() == kortti[3].getArvo() && kortti[0].getArvo() == 1)
                {
                    setVoitto(kortti[0].getArvo() + 140 + 13);
                }
                else 
                {
                    setVoitto(kortti[2].getArvo() + 140);
                }

            }
            if (kortti[1].getArvo() == (kortti[0].getArvo() + 1) && kortti[2].getArvo() == (kortti[0].getArvo() + 2) && kortti[3].getArvo() == (kortti[0].getArvo() + 3) && kortti[4].getArvo() == (kortti[0].getArvo() + 4) && kortti[0].getMaa() == kortti[1].getMaa() && kortti[0].getMaa() == kortti[2].getMaa() && kortti[0].getMaa() == kortti[3].getMaa() && kortti[0].getMaa() == kortti[3].getMaa() || kortti[2].getArvo() == (kortti[1].getArvo() + 1) && kortti[3].getArvo() == (kortti[1].getArvo() + 2) && kortti[4].getArvo() == (kortti[1].getArvo() + 3) && kortti[4].getArvo() == (kortti[0].getArvo() + 12) && kortti[0].getMaa() == kortti[1].getMaa() && kortti[0].getMaa() == kortti[2].getMaa() && kortti[0].getMaa() == kortti[3].getMaa() && kortti[0].getMaa() == kortti[3].getMaa())
            {
                tulos = "Varisuora";
                setVoitto(kortti[4].getArvo() + 160);
            }
            
            //laittaa suurimman voiton muistiin jotta Poyta-luokan metodi osaa antaa oikean String-nimen voittavalle kädelle
            if (muistavoitto < getVoitto() )
            {
                voittokasi = tulos;
                muistavoitto = getVoitto();
                Console.WriteLine(voittokasi);
            }
            
            //Console.WriteLine(muistavoitto);
            return tulos;
        }


    }
}
