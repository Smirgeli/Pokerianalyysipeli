﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pokerianalyysipeli
{
    class Kirjoittaja
    {
        private String filepath = Environment.ExpandEnvironmentVariables(@"%PUBLIC%\Analyysi.txt");
        //tulostaa annetun merkkijonon tekstitiedostoon
        public void tekstintulostus(String Z)
        {
            using (System.IO.StreamWriter file = new System.IO.StreamWriter(filepath, true))
            {
                file.WriteLine(Z);
            }
        }
        public void tekstintulostuseirivia(String W)
        {
            using (System.IO.StreamWriter file = new System.IO.StreamWriter(filepath, true))
            {
                file.Write(W);
            }
        }
    }
}
