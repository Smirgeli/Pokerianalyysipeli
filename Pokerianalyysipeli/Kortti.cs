﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pokerianalyysipeli
{
    class Kortti
    {
        private int Arvo;
        private String Maa;

        public Kortti ()
        {

        }

        //metodi joka käyttää piilotettuja metodeja asettamaan arvot
        public Kortti(int KArvo, String KMaa)
        {
            setArvo(KArvo);
            setMaa(KMaa);
        }

        //palauttaa Arvon
        public int getArvo()
        {
            return Arvo;
        }

        //asettaa Arvon
        public void setArvo(int KArvo)
        {
                Arvo = KArvo;
        }

        //Palauttaa Maan
        public String getMaa()
        {
            return Maa;
        }

        //Asettaa Maan
        public void setMaa(String KMaa)
        {
            Maa = KMaa;
        }
    }
}
