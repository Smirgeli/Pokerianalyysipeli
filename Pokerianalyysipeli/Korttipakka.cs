﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pokerianalyysipeli
{
    class Korttipakka
    {

        private Kortti[] korttipakka;
        private static Random random = new Random();

        //Alustaessa luo yhden korttipakan Kortti-olioista ja asettaa niille Maan sekä Arvon
        public Korttipakka(int maara)
        {
            korttipakka = new Kortti[maara * 52];
            String Maa;
            for (int z = 0; z < 4; z++)
                {
                for (int i = 0; i < 13; i++)
                    {
                    if (z == 0)
                    {
                        Maa = "Pata";
                        korttipakka[i] = new Kortti(i + 1, Maa);
                    }

                    if (z == 1)
                    {
                        Maa = "Ruutu";
                        korttipakka[i + (z * 13)] = new Kortti(i + 1, Maa);
                    }

                    if (z == 2)
                    {
                        Maa = "Risti";
                        korttipakka[i + (z * 13)] = new Kortti(i + 1, Maa);
                    }
                    if (z == 3)
                    {
                        Maa = "Hertta";
                        korttipakka[i + (z * 13)] = new Kortti(i + 1, Maa);
                    }
                    else
                    {
                       
                    }


                }
                }
        }
        
        //Sekoittaa pakan pseudorandomiin järjestykseen
        public Kortti[] Sekoitus(Kortti[] X)
        {
            Kortti[] tkorttipakka = new Kortti[korttipakka.Length];

                for (int z = korttipakka.Length - 1; z >= 0; z--)
                {
                tkorttipakka[z] = X[z];
                int randomIndex = random.Next(z + 1);
                X[z] = X[randomIndex];
                X[randomIndex] = tkorttipakka[z];
                }
            return tkorttipakka;
        }
        
        //ajaa Sekoituksen 3 kertaa, koska Sekoitus-metodi ei sekoita kortteja True-randomiin järjestykseen
        public void Sekoita()
        {
            Sekoitus(korttipakka);
            Sekoitus(korttipakka);
            Sekoitus(korttipakka);
        }

        //palauttaa korttipakan
        public Kortti[] GetPakka()
        {
            return korttipakka;
        }

    }
}


