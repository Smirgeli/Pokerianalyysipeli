﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pokerianalyysipeli
{
    class Pelaaja
    {
        private Kortti[] kasi = new Kortti[5];
        String kasikortit;
        public Pelaaja()
        {
            
        }

        //Asettaa yhden kortin arvon kädessä käyttämällä piilotettua metodia
        public void OtaKortti(Kortti kortti, int loopnr)
        {
            setKasi(kortti, loopnr);
        }

        //Tulostaa korttien arvot ja maat konsoliin, sekä järjestää käden suuruus järjestykseen
        public void getKortit()
        {
            Array.Sort(kasi, delegate (Kortti kortti1, Kortti kortti2) 
            {
                return kortti1.getArvo().CompareTo(kortti2.getArvo());
            });

            foreach (Kortti kortti in kasi)
            {
                if (kortti.getArvo() == 1)
                {
                    Console.Write(kortti.getMaa() + " " + "A" + " ");
                    kasikortit = kasikortit + kortti.getMaa() + " " + "A" + " ";
                }
                else if (kortti.getArvo() == 11)
                {
                    Console.Write(kortti.getMaa() + " " + "J" + " ");
                    kasikortit = kasikortit + kortti.getMaa() + " " + "J" + " ";
                }
                else if (kortti.getArvo() == 12)
                {
                    Console.Write(kortti.getMaa() + " " + "Q" + " ");
                    kasikortit = kasikortit + kortti.getMaa() + " " + "Q" + " ";
                }
                else if (kortti.getArvo() == 13)
                {
                    Console.Write(kortti.getMaa() + " " + "K" + " ");
                    kasikortit = kasikortit + kortti.getMaa() + " " + "K" + " ";
                }
                else
                {
                    Console.Write(kortti.getMaa() + " " + kortti.getArvo() + " ");
                    kasikortit = kasikortit + kortti.getMaa() + " " + kortti.getArvo() + " ";
                }

            }
            Console.WriteLine();
        }

        //piilotettu asetus metodi
        private void setKasi(Kortti kortti, int loopnr)
        {
            kasi[loopnr] = kortti;
        }

        //palauttaa taulukossa numeron kohdalla olevan kortin
        public Kortti getKortti(int loopnr)
        {
                 return kasi[loopnr];
        }
        //palauttaa kasikortit
        public String getKasikortit()
        {
            return kasikortit;
        }
        //tyhjentää kasikortit toisesta luokasta
        public void tyhjenna()
        {
            kasikortit = "";
        }
        
    }
}
