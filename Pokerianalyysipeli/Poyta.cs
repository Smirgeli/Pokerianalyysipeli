﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pokerianalyysipeli
{
    class Poyta
    {
        public Pelaaja[] pelaajat;
        private String voittokasi;
        List<String> lista = new List<String>();
        List<String> kasilista = new List<String>();
        private Kortti[] kortit;
        private Kortti[] tarkistettavatkortit = new Kortti[5];
        private Jakaja jakaja;
        //private int lkm;
        //private String v;
        private int nostetutkortit = 0;
        private Kirjoittaja kirjoittaja = new Kirjoittaja();
        private int[] kadenarvo;
        private List<int> muisti = new List<int>();
        private List<int> muistivali = new List<int>();
        private int[] voittajat;
        private int samaanro = 0;

        public Poyta()
        {

        }

        //Sekoittaa kortit ja ottaa jakajan metodilta korttien järjestyksen omaan muuttujaan muistiin
        public void Korttienjako()
        {

            jakaja.SekoitaKortit();
            kortit = jakaja.getKortit();



            for (int r = 0; r < pelaajat.Length; r++)
            {
                for (int t = 0; t < 5; t++)
                {
                    pelaajat[r].OtaKortti(kortit[nostetutkortit], t);
                    nostetutkortit++;
                }

            }

            nostetutkortit = 0;


        }

        //metodi joka ajetaan jokaisen kierroksen alussa, tämä asettaa pelaajien lukumäärän ja alustaa Jakaja, sekä Pelaaja oliot
        public void AsetaPelaajat(int lkm)
        {
            for (Boolean g = false; g != true;)
            {
                        jakaja = new Jakaja(1);
                        pelaajat = new Pelaaja[lkm];
                        kadenarvo = new int [pelaajat.Length];
                        voittajat = new int[pelaajat.Length];

                        for (int y = 0; y < lkm; y++)
                        {
                            pelaajat[y] = new Pelaaja();
                        }


                        g = true;
                        Console.WriteLine();
            }
        }

        //tulostaa jokaisen pelaajan käsikortit konsoliin
        public void tulostaKortit()
        {
            for (int t = 0; t < pelaajat.Length; t++)
            {
                kirjoittaja.tekstintulostus("Pelaajan #" + (t + 1) + " " + "kasi:");
                Console.WriteLine("Pelaajan #" + (t+1) + " " + "kasi:");
                pelaajat[t].getKortit();
                kirjoittaja.tekstintulostus(pelaajat[t].getKasikortit());
                lista.Add(pelaajat[t].getKasikortit());
                pelaajat[t].tyhjenna();
            }
            kirjoittaja.tekstintulostus("");

            Console.WriteLine();
        }
        //palauttaa arvon listasta
        public String getKasikortit(int x)
        {
            return lista[x];
        }
        //tyhjentaa listan uutta peliä varten
        public void tyhjennaLista()
        {
            lista.Clear();
        }
        //palauttaa käden nimen listasta
        public String getVoitto(int y)
        {
            return kasilista[y];
        }
        //tyhjentaa kasikorttilistan uutta peliä varten
        public void tyhjennaKasilista()
        {
            kasilista.Clear();
        }
        //tekee analyysin jokaiselle pelaajalle
        public void Analyysi()
        {
            for (int t = 0; t < pelaajat.Length; t++)
            {
                for (int e = 0; e < tarkistettavatkortit.Length; e++)
                {
                    tarkistettavatkortit[e] = pelaajat[t].getKortti(e);
                }
                kirjoittaja.tekstintulostus("Pelaajalla #" + (t + 1) + " " + "on:");
                Console.WriteLine("Pelaajalla #" + (t + 1) + " " + "on:");
                kirjoittaja.tekstintulostus(jakaja.TeeAnalyysi(tarkistettavatkortit));
                Console.WriteLine(jakaja.TeeAnalyysi(tarkistettavatkortit));
                kadenarvo[t] = jakaja.getVoitto();
                kasilista.Add(jakaja.TeeAnalyysi(tarkistettavatkortit));              
            }
            muisti.Clear();
            //katsoo kuinka monella pelaajalla on samanarvoinen käsi suurimman käden kanssa
            for (int q = 0; q < kadenarvo.Length; q++)
            {
                samaanro = kadenarvo.Max();
                if (kadenarvo[q] == samaanro)
                {
                    muisti.Add(q+1);
                    muistivali.Add(q + 1);
                }
                

            }
            //tulostaa voittajan numeron/voittajien numerot
                if (muisti.Count < 2)
            {
                Console.WriteLine("Voittaja: #" + muisti[0]);
                kirjoittaja.tekstintulostus("Voittaja: #" + muisti[0]);
            }
            else
            {
                Console.WriteLine("Tasapelin voittajat: ");
                kirjoittaja.tekstintulostus("Tasapelin voittajat: ");

                for (int u = 0; u < muisti.Count; u++)
                {
                    Console.Write("#" + (muisti[u]) + " ");
                    kirjoittaja.tekstintulostuseirivia("#" + (muisti[u]) + " ");
                }
            }
            
            muisti.Clear();
            jakaja.tyhjennaMuistavoitto();
            kirjoittaja.tekstintulostus("");
            kirjoittaja.tekstintulostus("");
        }
        //palauttaa voittajien nimet listasta joka on sisällöltään täysin sama kuin muisti-lista mutta ilman, että estää sen tyhjentämisen
        public List<int> getMuisti()
        {
            return muistivali;
        }
        //palauttaa voittavan käden nimen jakaja-luokasta
        public String getVoittokasi()
        {
            voittokasi = jakaja.getVoittokasi();
            return voittokasi;
        }
        //tyhjentää valimuisti listan
        public void tyhjennaMuistivali()
        {
            muistivali.Clear();
        }
    }
}
